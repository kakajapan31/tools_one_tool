# tools_one_tool

- profile info: ../tools_one_config/profile_info.txt
- chrome driver path: ../chrome_driver.exe
- list profile: dontpad.com/tools_one_ytb_profile
- list channel: dontpad.com/tools_one_ytb_channel_list

### profile info json:
- time_view_min: Optional[int] = 180: thời gian xem 1 video min
- time_view_max: Optional[int] = 300: thời gian xem 1 video max
- filter_1h: Optional[bool] = False: có filter trong 1h hay không?
- video_url: Optional[str] = '': link video dành cho option 2
- time_wait_to_view: Optional[int] = 0: thời gian đợi trước khi xem

- like_rate: Optional[int] = 0: tỉ lệ like 
- subscribe_rate: Optional[int] = 0: tỉ lệ subscribe 

- username: Optional[str] = None
- password: Optional[str] = None
- recover_email: Optional[str] = None

- channel_url: Optional[str] = None
- user_agent: Optional[str] = None
- content_search: Optional[str] = None
- channel_name: Optional[str] = ''
- cookies: Optional[str] = None
- browser: Optional[str] = 'chrome'

- proxy_url: Optional[str] = None
- proxy_user: Optional[str] = None
- proxy_pass: Optional[str] = None
- time_to_see_all: Optional[int] = 7200
- live_stream: Optional[bool] = False

- auto_captcha: Optional[bool] = False
- chrome_portable_path: Optional[str] = ''

- option: Optional[int] = 1

- option_1_amount_extra_video: Optional[int] = 0: số lượng video sau khi xem video đầu của option 1